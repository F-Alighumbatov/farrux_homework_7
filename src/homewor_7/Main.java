package homewor_7;


public class Main {


    public static void main(String[] args) {
        Dog dog = new Dog("jerry", 15, 75, new String[]{"eat", "drink", "sleep"});
        Fish fish = new Fish("jerry", 15, 75, new String[]{"eat", "drink", "sleep"});
        DomesticCat domestic_Cat = new DomesticCat("jerry", 15, 75, new String[]{"eat", "drink", "sleep"});
        RoboCat robo_Cat = new RoboCat("ROBOT_jerry");
        UnknownAnimal unknownAnimal = new UnknownAnimal("sad", Species.UNKNOWN);
        Man father = new Man("john", "Karleon", 1967);
        Woman mother = new Woman("Rosy", "Karleon", 1957);
        Man firstSon = new Man("Jack", "Karleon", 2002, 105, mother, father, dog);
        Man secondSon = new Man("Peter", "Karleon", 2006, 98, mother, father, robo_Cat);
        Woman daughter = new Woman("Rosaly", "Karleon", 2011, 99, mother, father, domestic_Cat);
        Man last_Born_Cild = new Man("Alex", "Karleon", 2014, 75, mother, father, fish);

        Dog dog1 = new Dog("jerry", 15, 75, new String[]{"eat", "drink", "sleep"});
        Family familyOfKarleon = new Family(father, mother);
        familyOfKarleon.addChild(firstSon);
        familyOfKarleon.addChild(secondSon);
        familyOfKarleon.addChild(daughter);
        familyOfKarleon.addChild(last_Born_Cild);
        System.out.println(familyOfKarleon);
        firstSon.greetPet();
        dog.foul();
        robo_Cat.respond();
        System.out.println(secondSon.greetPet());
        familyOfKarleon.deleteChild(secondSon);
        System.out.println(familyOfKarleon);
        mother.makeUp();
        father.repairCar();

    }

}
