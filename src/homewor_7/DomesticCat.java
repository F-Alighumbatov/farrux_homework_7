package homewor_7;

public class DomesticCat extends Pet implements Foul {
    @Override
    public void foul() {
        System.out.println("DomesticCat need to cover it up");
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOMESTIC_CAT);
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am ".concat(getNickname()).concat(". I miss you!"));
    }
}
