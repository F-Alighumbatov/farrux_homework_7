package homewor_7;


public class Dog extends Pet implements Foul {


    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOG);
    }

    @Override
    public void foul() {
        System.out.println("Dog need to cover it up");
    }

    @Override
    public void eat() {
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am ".concat(getNickname()).concat(". I miss you!"));
    }
}
