package homewor_7;


public enum Species {
    UNKNOWN,
    DOG(false, 4, true),
    DOMESTIC_CAT(false, 4, true),
    RABBIT(false, 4, true),
    FISH(false, 0, true),
    BIRD(true, 2, true),
    HORSE(false, 4, true);


    private boolean flyOrNotFly;
    private int howLegsHaveGot;
    private boolean hasFur;


    Species(boolean flyOrNotFly, int howLegsHaveGot, boolean hasFur) {
        this.flyOrNotFly = flyOrNotFly;
        this.howLegsHaveGot = howLegsHaveGot;
        this.hasFur = hasFur;
    }

    Species() {

    }


    @Override
    public String toString() {
        return String.format("{" +
                "fly=" + flyOrNotFly +
                ", howLegsHaveGot=" + howLegsHaveGot +
                ", canSwim=" + hasFur +
                '}');
    }
}
