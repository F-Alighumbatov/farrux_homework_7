package homewor_7;

public class UnknownAnimal extends Pet {
    private Species species = Species.UNKNOWN;

    public UnknownAnimal(String nickname, Species species) {
        super(nickname);
        this.species = species;
    }

    public UnknownAnimal(String nickname, int age, int trickLevel, String[] habits, Species species) {
        super(nickname, age, trickLevel, habits);
        this.species = species;
    }

    public UnknownAnimal() {

    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am ".concat(getNickname()).concat(". I miss you!"));
    }
}

