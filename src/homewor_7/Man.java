package homewor_7;

public final class Man extends Human {
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Human mother, Human father, Pet pet) {
        super(name, surname, year, iq, mother, father, pet);
    }

    public void repairCar() {
        System.out.println(getName() + ", is repairing car!");
    }

    @Override
    public String greetPet() {
        return super.greetPet();
    }
}
