package homewor_7;

import java.util.Objects;
import java.util.Random;

public class Human {
    Random random = new Random();
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private String[][] schedule = new String[7][2];
    private Family family;


    public String getName() {
        return name;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;

    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();
        str.append(name != null && !name.isEmpty() ? "{name='" + name + "', " : "");
        str.append(surname != null && !surname.isEmpty() ? "surname='" + surname + "', " : "");
        str.append(year != 0 ? "year=" + year + ", " : "");
        str.append(iq != 0 ? "iq=" + iq + ", " : "");
        str.append(pet != null ? pet : "");
        str.append("}}");

        return str.toString();
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family);
    }


    public String greetPet() {
        return "Hello! ".concat(pet.getNickname());

    }

    public String describePet() {
        if (pet.getTrickLevel() > 50) {
            return "I have a ".concat(pet.getSpecies().toString().concat(", he is ")
                    .concat(String.valueOf(pet.getAge())).concat(" years old,")
                    .concat("he is very sly"));
        } else if (pet.getTrickLevel() <= 50) {
            return "I have a ".concat(pet.getSpecies().toString().concat(", he is ")
                    .concat(String.valueOf(pet.getAge())).concat(" years old,")
                    .concat("he is almost not sly"));
        }
        return describePet();
    }

    public boolean feedPet() {

        if (pet.getTrickLevel() > random.nextInt()) {
            System.out.println("Hmm...I will feed ".concat(pet.getNickname()).concat("'s "));
            return true;
        } else System.out.println("I think ".concat(pet.getNickname()).concat("'s ")
                .concat(" is not hungary"));

        return false;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

}