package homewor_7;

import java.util.*;

public class Family {
    private int index;
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;
    private Family family;

    public Family(Human mother, Human father) {
//        this.mother = mother;
//        this.father = father;
        setMother(mother);
        setFather(father);
//        father.setFamily(this);
//        mother.setFamily(this);


    }

    @Override
    public String toString() {

        StringBuffer str = new StringBuffer();
        if (children.length <= 0) {
            str.append("This family havent got child");
            return str.toString();
        }
        str.append("Human{");
        str.append(father != null ? "Father: " + father.toString() : "");
        str.append(mother != null ? "Mother: " + mother.toString() : "");

        str.append(children != null ? Arrays.deepToString(children) : "");
        str.delete(7, 7);
        str.delete(str.length() - 3, str.length() - 1);
        str.append("}}");

        return str.toString();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] child) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father) && children.equals(family.children);

    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    public int countFamily() {
        int count = children.length + 2;
        System.out.println("The family members are: ".concat(String.valueOf(count)));
        return count;
    }

    public void addChild(Human human) {
        Human[] tempChildrenArray = new Human[children.length + 1];
        System.arraycopy(children, 0, tempChildrenArray, 0, children.length);
        tempChildrenArray[tempChildrenArray.length - 1] = human;
        human.setFamily(this);
        children = tempChildrenArray;

    }

    public boolean deleteChild(Human human) {
        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (human.equals(children[i])) {
                index = i;
            }
        }
        if (index == -1) {
            return false;
        } else {
            Human[] tempChildrenArray = new Human[children.length - 1];
            int j = 0;
            for (Human child : children) {

                if (human.equals(child)) {
                    child.setFamily(null);
                    continue;
                } else {

                    tempChildrenArray[j] = child;
                    j++;
                }
                children = tempChildrenArray;
            }
        }
        return true;
    }


    public boolean deleteChildWithIndex(int index) {

        index = index - 1;
        boolean delete = false;
        int a = 0;
        Human[] arrayChild = new Human[children.length - 1];
        for (int i = 0; i < children.length; i++) {
            if (!(index == i)) {
                arrayChild[a++] = children[i];
            }
        }
        this.children = arrayChild;
        if (arrayChild.length < children.length) {
            delete = true;
        }
        return delete;
    }

}
