package homewor_7;

public class RoboCat extends Pet {

    public RoboCat(String nickname) {
        super(nickname);
        super.setSpecies(Species.UNKNOWN);
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am ".concat(getNickname()).concat(". I miss you!"));
    }
}
