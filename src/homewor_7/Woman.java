package homewor_7;

public final class Woman extends Human {
    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Human mother, Human father, Pet pet) {
        super(name, surname, year, iq, mother, father, pet);
    }

    public void makeUp() {
        System.out.println(getName() + ", is doing make up!");
    }

    @Override
    public String greetPet() {
        return super.greetPet();
    }
}
